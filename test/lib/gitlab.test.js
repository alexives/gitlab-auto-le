const GitLab = require('../../lib/gitlab')
const MockAdapter = require('axios-mock-adapter')
var axios = require('axios')

var mock = new MockAdapter(axios)

const options = {
  token: 'foo',
  domain: 'bar.example.com',
  gitlabUrl: 'https://gitlab.example.com',
  apiUrl: 'https://gitlab.example.com/api/v4',
  project: '1234',
  branch: 'master'
}

describe('gitlab api interaction', () => {

  beforeEach(() => {
    mock.reset()
  })

  describe('updateCertificate', () => {
    it('calls the file pages domains api to update the certificate on a given domain', done => {
      mock.onPut('https://gitlab.example.com/api/v4/projects/1234/pages/domains/bar.example.com', {
        'certificate': 'a certificate file contents',
        'key': 'a private key file contents'
      }).reply(200)
      GitLab.updateCertificate(options, 'a private key file contents', 'a certificate file contents').then((response) => {
        done();
      })
    })
  });
  describe('createChallengeFile', () => {
    it('calls the file creation api to create the file with provided contents', done => {
      mock.onPost('https://gitlab.example.com/api/v4/projects/1234/files/path/to/file', {
        'branch': 'master',
        'content': 'foo bar baz',
        'commit_message': 'Challenge file for lets encrypt certificate for bar.example.com'
      }).reply(201)
      GitLab.createChallengeFile(options, 'path/to/file', 'foo bar baz').then((response) => {
        done();
      })
    })
  });
  describe('removeChallengeFile', () => {
    it('calls the file removeal api to create the file with provided contents', done => {
      mock.onDelete('https://gitlab.example.com/api/v4/projects/1234/files/path/to/file').reply(200)
      GitLab.removeChallengeFile(options, 'path/to/file', 'foo bar baz').then((response) => {
        done();
      })
    })
  });
});
