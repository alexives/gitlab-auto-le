const options = require('../../lib/options')

process.env.CI_PROJECT_ID = '1234'

describe('parsing arguments into options', () => {
  it('raises an error if no domain is provided', () => {
    expect(() => { options(['--token', 'foo']) }).toThrow('missing domain')
  });

  it('raises an error if no token is provided', () => {
    expect(() => { options(['--domain', 'bar.example.com']) }).toThrow('missing api token')
  });

  it('takes the domain from after --', () => {
    expect(options(['--token', 'foo', '--', 'baz.example.com'])).toEqual({
      _: ['baz.example.com'],
      token: 'foo',
      domain: 'baz.example.com',
      gitlabUrl: 'https://gitlab.com',
      apiUrl: 'https://gitlab.com/api/v4',
      project: '1234',
      branch: 'master'
    });
  })

  it('returns an object with api_url, token, gitlab_url, and domain', () => {
    expect(options(['--token', 'foo', '--domain', 'bar.example.com'])).toEqual({
      _: [],
      token: 'foo',
      domain: 'bar.example.com',
      gitlabUrl: 'https://gitlab.com',
      apiUrl: 'https://gitlab.com/api/v4',
      project: '1234',
      branch: 'master'
    });
  });

  it('updates the gitlab url when provided with one', () => {
    expect(options(['--token', 'foo', '--domain', 'bar.example.com', '--gitlabUrl', 'https://gitlab.example.com'])).toEqual({
      _: [],
      token: 'foo',
      domain: 'bar.example.com',
      gitlabUrl: 'https://gitlab.example.com',
      apiUrl: 'https://gitlab.example.com/api/v4',
      project: '1234',
      branch: 'master'
    });
  });

  it('updates the project when provided with one', () => {
    expect(options(['--project', 'project', '--token', 'foo', '--domain', 'bar.example.com'])).toEqual({
      _: [],
      token: 'foo',
      domain: 'bar.example.com',
      gitlabUrl: 'https://gitlab.com',
      apiUrl: 'https://gitlab.com/api/v4',
      project: 'project',
      branch: 'master'
    });
  });
});
