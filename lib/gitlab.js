const axios = require('axios')

module.exports = {
  updateCertificate: (options, key, cert) => {
    return axios({
      method: 'put',
      url: `${options.apiUrl}/projects/${options.project}/pages/domains/${options.domain}`,
      data: {
        certificate: cert,
        key: key
      }
    })
  },
  createChallengeFile: (options, filename, contents) => {
    return axios({
      method: 'post',
      url: `${options.apiUrl}/projects/${options.project}/files/${filename}`,
      data: {
        branch: options.branch,
        content: contents,
        commit_message: `Challenge file for lets encrypt certificate for ${options.domain}`
      }
    })
  },
  removeChallengeFile: (options, filename, contents) => {
    return axios({
      method: 'delete',
      url: `${options.apiUrl}/projects/${options.project}/files/${filename}`,
      data: {
        branch: options.branch,
        commit_message: `Remove challenge file for lets encrypt certificate for ${options.domain}`
      }
    })
  } 
}