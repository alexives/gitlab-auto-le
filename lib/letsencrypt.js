const ACME = require('le-acme-core').ACME.create()
const NodeRSA = require('node-rsa')
var key = new NodeRSA();
var accountKeyPair = key.generateKeyPair(1024, 65537)
var domainKeyPair = key.generateKeyPair(1024, 65537)

module.exports = {  
  accountKeyPair: accountKeyPair,
  domainKeyPair: domainKeyPair
}
