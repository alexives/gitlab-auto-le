const minimist = require('minimist');

module.exports = (args) => {
  var args = minimist(args)

  args.gitlabUrl = args.gitlabUrl || 'https://gitlab.com'
  args.apiUrl = `${args.gitlabUrl}/api/v4`
  args.project = args.project || process.env.CI_PROJECT_ID
  args.branch = args.branch || 'master'
  args.domain = args.domain || args._[0]

  if(args.token == undefined) { throw 'missing api token' }
  if(args.domain == undefined) { throw 'missing domain' }
  if(args.project == undefined) { throw 'missing project id in args or CI_PROJECT_ID' }

  return args
}